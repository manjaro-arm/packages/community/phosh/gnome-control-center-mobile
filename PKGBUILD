# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

# Mobian patches: https://salsa.debian.org/Mobian-team/packages/gnome-control-center/-/tree/mobian/debian/patches

pkgbase=gnome-control-center-mobile
pkgname=(
  gnome-control-center-mobile
  gnome-keybindings-mobile
)
pkgver=46.4
pkgrel=1
pkgdesc="GNOME's main interface to configure various aspects of the desktop - with mobile patches"
url="https://gitlab.gnome.org/GNOME/gnome-control-center.git"
license=(GPL-2.0-or-later)
arch=(x86_64 armv7h aarch64)
depends=(
  accountsservice
  bolt
  cairo
  colord-gtk4
  cups-pk-helper
  dconf
  fontconfig
  gcc-libs
  gcr-4
  gdk-pixbuf2
  glib2
  glibc
  gnome-bluetooth-3.0
  gnome-color-manager
  gnome-desktop-4
  gnome-online-accounts
  gnome-settings-daemon
  gnome-shell
  gnutls
  graphene
  gsettings-desktop-schemas
  gsound
  gtk4
  hicolor-icon-theme
  json-glib
  krb5
  libadwaita
  libcolord
  libcups
  libepoxy
  libgoa
  libgtop
  libgudev
  libibus
  libmalcontent
  libmm-glib
  libnm
  libnma-gtk4
  libpulse
  libpwquality
  libsecret
  libsoup3
  libwacom
  libx11
  libxi
  libxml2
  pango
  polkit
  smbclient
  sound-theme-freedesktop
  tecla
  udisks2
  upower
)
makedepends=(
  docbook-xsl
  git
  glib2-devel
  meson
  modemmanager
)
checkdepends=(
  python-dbusmock
  python-gobject
  xorg-server-xvfb
)
source=(
  "git+https://gitlab.gnome.org/GNOME/gnome-control-center.git?signed#tag=$pkgver"
  'git+https://gitlab.gnome.org/GNOME/libgnome-volume-control.git'

   # Upstream backports
   keyboard-Allow-disabling-alternate-characters-key.patch
   Expose-touchpad-settings-if-synaptics-is-in-use.patch
   Ignore-result-of-test-network-panel.patch

   # Purism's patches
   0001-avatar-chooser-Adapt-to-work-on-librem5.patch
   Add-patches-to-check-if-phone.patch

   # Mobian patches
   0004-power-add-more-suspend-timing-options.patch
   resize-connection-editor.patch

   # Manjaro's patches
   software-updates.patch
   manjarolinux-text-rounded.svg
   manjarolinux-text-dark-rounded.svg
)

sha256sums=('a60dfef176c3dafe80c2ee4726240f1a6fd1c98ebec07541760e83068013cdf0'
            'SKIP'
            '429fe942fc2544e89a203c806dc32a76928c06b721fe23f52be6d6a9b0d0a53f'
            '7ac1dc01e624c002e4d8df98b4273b3030422e1780b281b29b45ea148e6e2203'
            'a0f820cf717b3b095f9fd64d8cfb960032bb1a46d78fb8731cc9e879d54a7645'
            'c18c1b9fdab8e8b7aad8519a58996267377965dbe3d8d8dc9a9129fe80da01ef'
            '09e36b0aa2c40d6b6f5be11fd9617223d5b142ff8b50db6fce0eeebc63007327'
            '6873d59fdcb00547339083a0a5a0213f2a975acc2f50b093e2efd62bab95ac21'
            'c006215f8e8c6ef523708089f246815f845d82d94e5f442460d4a4aafdfb617b'
            'cafa53ec8988ef2da2a4b5aabaff05a1a60a053523b1cc854ed1054991894372'
            '1a7e96218a9d75243f65b6689f4e21220dd25c9b6610a4a9ffed815c2b8a9ea1'
            '5d49f926a6b05fd9a8a9e4f5478bb47c94e9e5b62dfa14913341104b3a454da7')
validpgpkeys=(
  9B60FE7947F0A3C58136817F2C2A218742E016BE # Felipe Borges (GNOME) <felipeborges@gnome.org>
)

pkgver() {
  cd gnome-control-center
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd gnome-control-center

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done

  git submodule init subprojects/gvc
  git submodule set-url subprojects/gvc "$srcdir/libgnome-volume-control"
  git -c protocol.file.allow=always -c protocol.allow=never submodule update
}

build() {
  local meson_options=(
    -D documentation=true
    -D location-services=enabled
    -D malcontent=true
    -D distributor_logo=/usr/share/icons/manjaro/manjarolinux-text-rounded.svg
    -D dark_mode_distributor_logo=/usr/share/icons/manjaro/manjarolinux-text-dark-rounded.svg
  )

  arch-meson gnome-control-center build "${meson_options[@]}"
  meson compile -C build
}

check() {
  GTK_A11Y=none dbus-run-session xvfb-run -s '-nolisten local +iglx -noreset' \
    meson test -C build --print-errorlogs
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_gnome-control-center-mobile() {
  depends+=(gnome-keybindings-mobile)
  optdepends=(
    'fwupd: device security panel'
    'gnome-remote-desktop: screen sharing'
    'gnome-user-share: WebDAV file sharing'
    'malcontent: application permission control'
    'networkmanager: network settings'
    'openssh: remote login'
    'power-profiles-daemon: power profiles'
    'rygel: media sharing'
    'system-config-printer: printer settings'
  )
  provides=(gnome-control-center)
  conflicts=(gnome-control-center)

  meson install -C build --destdir "$pkgdir"
  install -d -o root -g 102 -m 750 "$pkgdir/usr/share/polkit-1/rules.d"
  echo "X-Purism-FormFactor=Workstation;Mobile;" >> "$pkgdir/usr/share/applications/gnome-control-center.desktop"

  install -Dm644 "$srcdir"/manjarolinux{-text,-text-dark}-rounded.svg -t \
    "$pkgdir/usr/share/icons/manjaro/"

cd "$pkgdir"
  _pick gkb usr/share/gettext/its/gnome-keybindings.*
  _pick gkb usr/share/gnome-control-center/keybindings
  _pick gkb usr/share/pkgconfig/gnome-keybindings.pc
}

package_gnome-keybindings-mobile() {
  pkgdesc="Keybindings configuration for GNOME applications"
  depends=()
  provides=(gnome-keybindings)
  conflicts=(gnome-keybindings)

  mv gkb/* "$pkgdir"
}

# vim:set sw=2 sts=-1 et:

